package supporthero.io

import com.opensymphony.module.sitemesh.Decorator
import com.opensymphony.module.sitemesh.Page
import org.codehaus.groovy.grails.web.sitemesh.GrailsLayoutDecoratorMapper

import javax.servlet.http.HttpServletRequest

class LayoutDecoratorMapper extends GrailsLayoutDecoratorMapper {

    public Decorator getDecorator(HttpServletRequest request, Page page) {
        Decorator decorator = super.getDecorator(request, page)
        if (request.parameterMap.containsKey('layout')) {
            String container = request.getParameter('layout');
            log.debug "Serving partial layout: ${container}"
            decorator = getNamedDecorator(request, "_application-${container}")
        }
        return decorator
    }

}
