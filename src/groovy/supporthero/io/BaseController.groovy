package supporthero.io

import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.support.RequestContextUtils
import supporthero.io.util.InetAddressUtil

abstract class BaseController {

    boolean initializeContext(boolean readOnly = true) {
        return true
    }

    String getUserIp() {
        InetAddressUtil.getAddress(request)
    }

    protected def renderTemplate(String name) {
        String viewName = name.tokenize('.').first()
        String templateName = name.tokenize('.').tail().join('/')
        render template: "/${viewName}/${templateName}", model: [context: context]
    }

}
