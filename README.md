supporthero-design
==================

1. Use Grails 2.4.4 in your terminal (you may use gvm to do so, which is pretty simple http://gvmtool.net/ --> gvm install grails 2.4.4)

2. Launch the app with the command : grails run-app

3. Go to http://localhost:8080/

There are 2 example views : Content and Statistics.
The goal here is to create the HELP CENTER Views : Home, Categories, Article and Search (find the corresponding html templates in PROJECT_FOLDER/grails-app/views/helpCenter/)

You may want to create a different navbar : see _navBar.gsp, included in layouts/application.gsp. You can create a _navBar2.gsp and insert it in application.gsp instead of _navBar.
Please try to keep the main markup of the _navBar.gsp template though.

All SASS ressources are located in PROJECT_FOLDER/resources/sass/
You may add new css rules, but not remove existing ones as they are currently used in the project.