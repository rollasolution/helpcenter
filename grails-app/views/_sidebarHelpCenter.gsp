<div class="row sidebar-row">
    <div class="">
        <div class="sidebar-getintouch">
            <h4>GET IN TOUCH</h4>
            <ul class="getintouch-items">
                <li>
                    <a href="#">CONTACT SUPPORT TEAM <i class="fa fa-long-arrow-right"></i></a>
                </li>
                <li>
                    <a href="#">CHAT WITH US <i class="fa fa-long-arrow-right"></i></a>
                </li>
                <li>
                    <a href="#">WHAT EVER THE LINK YOU WANT <i class="fa fa-long-arrow-right"></i></a>
                </li>
                <li>
                    <a href="#">27/7 CALL US 0 800 56 78 <i class="fa fa-long-arrow-right"></i></a>
                </li>
            </ul>
        </div>

        <div class="sidebar-categories">
            <h4>CATEGORIES</h4>
            <ul class="categories-items">
                <li>
                    <a href="#">Account</a>
                </li>
                <li>
                    <a href="#">Facebook PAGE</a>
                </li>

                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Publishing</a>
                </li>
                <li>
                    <a href="#">Fan Management</a>
                </li>
                <li>
                    <a href="#">Calendar</a>
                </li>
                <li>
                    <a href="#">Stats Facebook</a>
                </li>
                <li>
                    <a href="#">Competitors´ benchamarking</a>
                </li>
            </ul>
        </div>


        <div class="sidebar-powered">
            <span>
                <a><g:message code="help.sidebar.powered"/></a>
                <img  src="${assetPath(src: 'helpCenter/powered.png')}"/>
            </span>
        </div>

    </div>
</div>


