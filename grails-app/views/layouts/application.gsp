<%@ page import="grails.util.Environment" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><g:layoutTitle default="${pageProperty(name:'page.title') ?: 'SupportHero'}" /></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
  <asset:stylesheet href="supporthero.css"/>
  <asset:javascript src="supporthero.js"/>
  <supporthero:init/>

  <script type="text/javascript" src="//use.typekit.net/njh7kwg.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

  <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
  <!--[if lt IE 9]>
		  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.min.js"></script>
		<![endif]-->

  <g:render template="/layouts/custom-styling"/>
</head>
<body class="controller-{{view.controller}} action-{{view.action}}"
      id="supportHeroController">

<g:render template="/navBar" />

<div id="wrapper" data-ng-class="{'embedded': context.iFrameEnabled}">
    <g:render template="/layouts/application-wrapper"/>
</div>

<g:render template="/footer" />
</body>
</html>
