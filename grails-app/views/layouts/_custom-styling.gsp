<g:set var="accountColor" value="#009900"/>
<g:set var="accountLinkColor" value="#009900"/>
<g:set var="accountTitleColor" value="#ffffff"/>

<style>
    /*header*/
    .navbar-default,
    .nav-pills--context__search-block .btn { background:${accountColor} !important;} /*color background header*/
    .navbar .nav > li > a { text-shadow:0 1px 0 ${accountColor} !important;} /*color background header*/
    .navbar .nav > li > a,
    .navbar .nav > li > a:hover,
    .navbar .nav > li > a:focus,
    .navbar .nav li.dropdown.open > .dropdown-toggle,
    .navbar .nav li.dropdown.active > .dropdown-toggle,
    .navbar .nav li.dropdown.open.active > .dropdown-toggle,
    .nav-pills--context__search-block .btn { color:${accountTitleColor} !important;} /*color link header*/

    #nprogress .bar { background: ${accountTitleColor} !important; }
    #nprogress .peg { box-shadow: 0 0 10px ${accountTitleColor}, 0 0 5px ${accountTitleColor} !important; }
    #nprogress .spinner-icon {
        border-top-color:${accountTitleColor} !important;
        border-left-color:${accountTitleColor} !important;
    }

    .nav-pills--context > .active > a,
    .nav-pills--context > li > a:hover {
        border-bottom-color: ${accountLinkColor} !important;
    }

    .nav-pills--context__logo a svg {
        fill: ${accountColor};
    }

    .nav-pills--context__logo a:hover svg {
        fill: ${accountLinkColor} !important;
    }

    /* a, */
    .dropdown-menu > li > a:hover,
    .dropdown-menu > li > a:focus,
    .dropdown-submenu:hover > a,
    .dropdown-submenu:focus > a,
    .nav-pills > .active > a,
    .nav-pills > .active > a:hover,
    .nav-pills a:hover,
    .nav-pills--context > li a:hover,
    .nav > li .btn-action,
    .list-group-item__header:hover,
    .list-group-item__header.is-active,
    .list-group-item__header:hover .list-group__toogle,
    .list-group-item__header.is-active .list-group__toogle,
    .list-group-tutorial-item .media-link:hover,
    .list-group-tutorial-item__heading a:hover,
    .list-group-tutorial-item__heading a:hover span,
    .panel-group.collapsed .accordion-toggle:hover,
    .list-group .accordion-toggle:hover,
    .list-group .panel-heading:hover .content-type__icon,
    .media-body:hover,
    .lnk-tooltip:hover,
    .breadcrumb .back__link > a,
    .brand-color,
    .ui-match,
    .list-view-all-item__heading a,
    .list-view-all-item__heading a:hover,
    .list-view-all-item .media-type,
    .list-group--view-all .accordion-toggle:hover .content-type__icon,
    .list-group--view-all .accordion-toggle:hover span,
    .btn--add,
        /*.switch__wrapper ul li.active a,*/
    .sort__select__custom select,
    .sort__select__custom:after,
    .sort__view__link:hover,
    .sort__pager__link:hover,
    .control-item .control-item__edit:hover,
    .btn--add-url,
    .table--data.table--url tr:hover td:first-child,
    .table--data tbody tr:hover th,
    .table--data tbody tr:hover .content-type__icon,
    .breadcrumb.url-edit__breadcrumb .url-edit__breadcrumb__path .title,
    .breadcrumb.url-edit__breadcrumb .url-edit__breadcrumb__path .relative-url,
    .list--sidebar li:hover,
    .list--sidebar li:hover .fa,
    .btn-add-step,
    .btn-add-step:hover,
    .btn-add-step:before,
    .url__edit-view .sidebar__close:hover,
    .list-group-wrapper--url-edit .tools a:hover,
    .list--menu .list__item a.is-active,
    .list--menu .list__item a:hover,
    .view-all,
    .view-all:hover,
    .table--data.table--stats tbody tr:hover td:first-child,
    .table--data.table--stats tbody tr:hover .content-type__icon,
    .list--sort .list__item a.is-active,
    .list--sort .list__item a:hover,
    .table--data.table--stats--full thead th .details,
    .table--data.table--stats--full tr.is-title-active td:first-child,
    .table--data.table--stats--full tr.is-title-active .content-type__icon,
    .form--free__actions__preview:hover,
    .answers-block__custom-answer__row input[type="text"].input-primary::-webkit-input-placeholder,
    .btn--import--bordered,
    .modal--bigger .modal-btn-group p a,
    .switch__wrapper ul li a:hover,
    .sidebar__list .list__tree li.active > a:after,
    .field-step-type .field-step-type__choice:hover,
    .btn--add-url span,
    .table--data.table--url tr:hover .article-title,
    .btn--gradient,
    .form--view__item--done__controls a.goto-next,
    .form-view--answer-wrapper__controls.goto-next,
    .btn-form--box,
    .form--view__item--done__controls a:hover,
    .btn-form--search,
    .content a,
    .content a:hover,
    .btn--goto:hover,
    .btn--goto:focus,
    .btn--goto:active {
        color:${accountLinkColor} !important; /*link color*/
    }

    .nav-pills > .active > a:after,
    .nav-pills > .active > a:hover:after,
    .nav-pills >li a:hover:after,
    .list-group-tutorial-item__heading a:hover span:before,
    .nav-pills--context__search-block .btn:active,
        /* .switch__wrapper,*/
    .btn--sync,
    .modal--grey .link,
    .form--modal .form-group--linked:before,
    .btn-primary,
    .form--sidebar .form-group .btn,
    .step-manager__cursor:before,
    .step-manager__cursor:after,
    .btn--import,
    .switch__wrapper ul li.active,
    .sidebar__list .list__tree li.active > a,
    .field-step-type .field-step-type__choice.field-step-type__choice--active,
    .table--data.table--url tr:hover .btn--edit-url,
    .form--view__item--done input[type="radio"]:before {
        background:${accountLinkColor} !important; /*link color*/
    }

    .panel-inner a,
    #footer a,
        /* .switch__wrapper, */
    .btn--add,
    .sort__select__custom select,
    .btn-add-step,
    .btn--import--bordered,
    .btn--group:hover,
    .btn--group:active,
    .btn--group:focus,
    .btn--group.is-active,
    .modal--bigger .modal-btn-group p a,
    .table--data.table--url tr:hover .btn--edit-url,
    .form--view__item--done__controls a:hover,
    .content a,
    .content a:hover {
        border-color:${accountLinkColor} !important; /*link color*/
    }

    .navbar.navbar-alerted .container .navbar-alert:after{
        border-right-color: ${accountColor} !important;
    }

    /*.tooltip.top .tooltip-arrow { border-top-color:${accountLinkColor} !important;} link color*/

    /*.tooltip-inner {background: ${accountLinkColor} !important;}link color*/

</style>
