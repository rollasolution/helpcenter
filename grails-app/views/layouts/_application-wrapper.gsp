<div class="container">

    <g:if test="${pageProperty(name:'page.menuNav')}">
        <div class="main-nav">
            <g:pageProperty name="page.menuNav"/>
        </div>
    </g:if>
    <g:if test="${pageProperty(name:'page.menuHeader')}">
        <header class="header header--content">
            <g:pageProperty name="page.menuHeader"/>
        </header>
    </g:if>

    <div>
        <%-- Page main --%>
        <div id="mainWrapper">
            <div id="main">
                <g:layoutBody/>
            </div>
        </div>
    </div>

</div>

