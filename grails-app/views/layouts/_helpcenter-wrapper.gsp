<div class="container container-helpcenter">

    <div class="main-helpcenter">

        <g:render template="/navBarhelpcenter"/>

        <div class="main-content col-lg-12 col-md-12 col-xs-12">
            <g:layoutBody/>
            <div class="main-sidebar col-lg-3 col-md-3 col-xs-3">
                <g:render template="/sidebar"/>
            </div>
        </div>

    </div>

</div>

