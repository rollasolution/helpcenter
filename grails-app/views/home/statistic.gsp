<header class="header header--content">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-xs-10">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-3 breadcrumb content__breadcrumb">
                    Overview
                </div>
            </div>
        </div>
    </div>
</header>
<div class="row">
    <div class="col-lg-10 col-md-10 col-xs-10 stats__wrapper">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-xs-10 sidebar sidebar--left sidebar--stats">
                <ul class="list list--menu">
                    <li class="list__item">
                        <a href="#" class="is-active">Dashboard</a>
                    </li>
                    <li class="list__item">
                        <a href="#">Content</a>
                    </li>
                    <li class="list__item">
                        <a href="#">Keywords</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-8 col-md-8 col-xs-10 stats__content">
                <div class="panel panel--white panel--stats">
                    <ul class="list list--stats list--primary-stats">
                        <li class="list__item">
                            <span class="stat-label">Tickets saved</span>
                            <a href="#"><span class="stat-value green">45</span></a>
                        </li>
                        <li class="list__item">
                            <span class="stat-label">Failed searches</span>
                            <a href="#"><span class="stat-value red">289</span></a>
                        </li>
                        <li class="list__item">
                            <span class="stat-label black">Widget opening</span>
                            <a href="#"><span class="stat-value black">20,345</span></a>
                        </li>
                    </ul>
                    <ul class="list list--stats list--secondary-stats">
                        <li class="list__item">
                            <span class="stat-label">Articles read</span>
                            <a href="#"><span class="stat-value black">712</span></a>
                        </li>
                        <li class="list__item">
                            <span class="stat-label grey">Searches</span>
                            <a href="#"><span class="stat-value grey">1289</span></a>
                        </li>
                    </ul>
                </div>
                <div class="panel panel--required-attention">
                    <div class="panel__heading">
                        <h2>FAQ articles requiring your attention</h2>
                        <a href="#" class="info">
                            <i class="fa fa-info-circle"></i>
                        </a>
                        <a href="#" class="view-all">
                            View all
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                    <table class="table table--data table--stats">
                        <thead>
                        <tr>
                            <!-- For what's coming underneath -->
                            <!-- The icon class depends on the way you sort the data -->
                            <!-- "fa-caret-up" for ascending sort  -->
                            <!-- "fa-caret-down" for descending sort -->
                            <!-- Only appears when "is-active" class is added to the <a> -->
                            <th>
                                <a href="#">Articles<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                            <th class="centered">
                                <a href="#">Article quality<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <i class="fa fa-file-text-o content-type__icon"></i>How many times can a participant enter an instant win campaign?
                            </td>
                            <td class="centered red">
                                -156
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-file-text-o content-type__icon"></i>Can I export all the draw dates of all my Instant wins?
                            </td>
                            <td class="centered red">
                                -135
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                            </td>
                            <td class="centered red">
                                -80
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel panel--failed">
                    <div class="panel__heading">
                        <h2>Failed searches</h2>
                        <a href="#" class="info">
                            <i class="fa fa-info-circle"></i>
                        </a>
                        <a href="#" class="view-all">
                            View all
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                    <table class="table table--data table--stats">
                        <thead>
                        <tr>
                            <!-- For what's coming underneath -->
                            <!-- The icon class depends on the way you sort the data -->
                            <!-- "fa-caret-up" for ascending sort  -->
                            <!-- "fa-caret-down" for descending sort -->
                            <!-- Only appears when "is-active" class is added to the <a> -->
                            <th>
                                <a href="#">Keywords<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                            <th class="centered">
                                <a href="#"># of searches<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                            <th class="centered">
                                <a href="#"># of failed searches<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>campaign
                            </td>
                            <td class="centered value">
                                79
                            </td>
                            <td class="centered value">
                                79
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>Instant wins
                            </td>
                            <td class="centered value">
                                79
                            </td>
                            <td class="centered value">
                                79
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>comment fonctionne l'option de viralité sur le tirage au sort
                            </td>
                            <td class="centered value">
                                61
                            </td>
                            <td class="centered value">
                                61
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>Instant wins
                            </td>
                            <td class="centered value">
                                35
                            </td>
                            <td class="centered value">
                                35
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>dolor sit amet
                            </td>
                            <td class="centered value">
                                23
                            </td>
                            <td class="centered value">
                                23
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel panel--popular">
                    <div class="panel__heading">
                        <h2>Popular searches</h2>
                        <a href="#" class="info">
                            <i class="fa fa-info-circle"></i>
                        </a>
                        <a href="#" class="view-all">
                            View all
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                    <table class="table table--data table--stats">
                        <thead>
                        <tr>
                            <!-- For what's coming underneath -->
                            <!-- The icon class depends on the way you sort the data -->
                            <!-- "fa-caret-up" for ascending sort  -->
                            <!-- "fa-caret-down" for descending sort -->
                            <!-- Only appears when "is-active" class is added to the <a> -->
                            <th>
                                <a href="#">Keywords<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                            <th class="centered">
                                <a href="#"># of searches<i class="fa fa-caret-down sort-icon"></i></a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>campaign
                            </td>
                            <td class="centered value">
                                79
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>Instant wins
                            </td>
                            <td class="centered value">
                                79
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>comment fonctionne l'option de viralité sur le tirage au sort
                            </td>
                            <td class="centered value">
                                61
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>Instant wins
                            </td>
                            <td class="centered value">
                                35
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-search content-type__icon"></i>dolor sit amet
                            </td>
                            <td class="centered value">
                                23
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel panel--failed">
                    <div class="panel__heading">
                        <h2>Most viewed articles</h2>
                    </div>
                    <div class="panel__content">
                        <h2>Available soon, it's on our roadmap.</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
