<!-- <div class="content--list"> -->
<header class="header header--content">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-xs-10">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-3 breadcrumb content__breadcrumb">
                    Manage content

                    <div class="switch__wrapper">
                        <ul>
                            <li class="active">
                                <a href="#">List view</a>
                            </li>
                            <li>
                                <a href="#">Tree view</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="row">
<div class="col-lg-10 col-md-10 col-xs-10 content__manager">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-xs-10 content__list-view">
            <table class="table table--data">
                <thead>
                <tr>
                    <!-- For what's coming underneath -->
                    <!-- The icon class depends on the way you sort the data -->
                    <!-- "fa-caret-up" for ascending sort  -->
                    <!-- "fa-caret-down" for descending sort -->
                    <!-- Only appears when "is-active" class is added to the <a> -->
                    <th>
                        <a href="#">Content<i class="fa fa-caret-up sort-icon"></i></a>
                    </th>
                    <th class="centered">
                        <a href="#" class="is-active">Date<i class="fa fa-caret-up sort-icon"></i></a>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                    </td>
                    <td class="centered">
                        Oct 12 14
                        <div class="list-group-item__action">
                            <a href="#">
                                <i class="fa fa-pencil lnk-tooltip"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-trash-o lnk-tooltip"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                    </td>
                    <td class="centered">
                        Oct 12 14
                        <div class="list-group-item__action">
                            <a href="#">
                                <i class="fa fa-pencil lnk-tooltip"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-trash-o lnk-tooltip"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                    </td>
                    <td class="centered">
                        Oct 12 14
                        <div class="list-group-item__action">
                            <a href="#">
                                <i class="fa fa-pencil lnk-tooltip"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-trash-o lnk-tooltip"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                    </td>
                    <td class="centered">
                        Oct 12 14
                        <div class="list-group-item__action">
                            <a href="#">
                                <i class="fa fa-pencil lnk-tooltip"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-trash-o lnk-tooltip"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr class="last-entry">
                    <td>
                        <i class="fa fa-file-text-o content-type__icon"></i>Is there any risk of winding up with the same winner twice in ...
                    </td>
                    <td class="centered">
                        Oct 12 14
                        <div class="list-group-item__action">
                            <a href="#">
                                <i class="fa fa-pencil lnk-tooltip"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-trash-o lnk-tooltip"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-10 sidebar">
            <div class="sidebar__controls">
                <div class="control-item control-item--sync">
                    <a href="#" class="btn btn--sync btn--sync--full">
                        <i class="fa fa-refresh"></i>
                        Synchronize
                    </a>
                    <a href="#" class="control-item__edit">
                        Edit synchro settings
                        <i class="fa fa-long-arrow-right"></i>
                    </a>
                </div>
                <div class="control-item control-item--add">
                    <a href="#" class="btn btn--add btn--add--full">
                        <i class="fa fa-plus"></i>
                        Add an article
                    </a>
                    <a href="#" class="control-item__edit">
                        Manage categories
                        <i class="fa fa-long-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="sidebar__list">
                <h4 class="list__title">Types</h4>
                <ul class="list list__tree">
                    <li>
                        <a href="#">Tutorial</a>
                    </li>
                    <li>
                        <a href="#">Articles</a>
                    </li>
                </ul>
            </div>
            <div class="sidebar__list">
                <h4 class="list__title">Categories</h4>
                <ul class="list list__tree">
                    <li>
                        <a href="#">Swepstakes</a>
                    </li>
                    <li>
                        <a href="#">Photo contest</a>
                    </li>
                    <li class="active">
                        <a href="#">Quizz app</a>
                        <ul class="sublist__tree">
                            <li>
                                <a href="#">Quizz rules</a>
                            </li>
                            <li>
                                <a href="#">Quiz pricing</a>
                            </li>
                            <li>
                                <a href="#">Quiz lorem</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Statistics</a>
                    </li>
                    <li>
                        <a href="#">Account</a>
                    </li>
                    <li>
                        <a href="#">Billing</a>
                    </li>
                    <li>
                        <a href="#">Photo contest</a>
                    </li>
                </ul>
            </div>
            <div class="sidebar__list sidebar__list--last">
                <h4 class="list__title">Sources</h4>
                <ul class="list list__tree">
                    <li>
                        <a href="#">Native</a>
                    </li>
                    <li>
                        <a href="#">Imported</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-10 col-md-10 col-xs-10 content__sort">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-xs-10">
            <div class="sort__wrapper">
                <div class="sort__select">
                    <div class="sort__select__custom">
                        <select>
                            <option value="100">100</option>
                            <option value="50">50</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                    Items per page
                </div>
                <div class="sort__view">
                    <a href="#" class="sort__view__link">View all</a>
                </div>
                <div class="sort__pager">
                    <a href="#" class="sort__pager__link">Previous</a>
                    <span class="sort__pager__state">0-50</span>
                    <a href="#" class="sort__pager__link">Next</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- </div> -->

