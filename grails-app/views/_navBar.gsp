<%@ page import="grails.util.Environment" %>
<header id="header">

<!------------------------- Default NavBar ---------------------------------------->

<div class="navbar navbar-default">
    <div class="navbar-inner">
        <div class="container">
            <a class="header__logo" href="${createLink([action: 'content', controller: 'home'])}">
                <img alt="Supporthero.io"
                     src="${assetPath(src: 'support-hero-logo.svg')}"
                     height="27"
                     width="32"/>
            </a>

            <div class="header__nav">
                <ul class="nav navbar-nav">
                    <li>
                        <g:link action="content" controller="home">
                            <g:message code="menu.content.title"/>
                        </g:link>
                    </li>
                    <li>
                        <g:link action="statistic" controller="home">
                            <g:message code="menu.statistics.title"/>
                        </g:link>
                    </li>
                    <li>
                        <g:link action="home" controller="helpCenter">
                            Home
                        </g:link>
                    </li>
                    <li>
                        <g:link action="categories" controller="helpCenter">
                            Categories
                        </g:link>
                    </li>
                    <li>
                        <g:link action="article" controller="helpCenter">
                            Article
                        </g:link>
                    </li>
                    <li>
                        <g:link action="search" controller="helpCenter">
                            Search
                        </g:link>
                    </li>
                </ul>
            </div>

            <div class="pull-right header__nav">
                <ul class="nav pull-right navbar-nav">
                    <g:if test="${params.action != 'login'}">
                        <li>
                            <span class="lnk-drop" data-placement="bottom" data-animation="am-flip-x"
                                  data-template="signInPopover" data-bs-popover>
                                <g:message code="navbar.menu.signin"/>
                                <div class="signin-lnk__caret-wrapper">
                                    <span class="fa fa-caret-down fa-1"></span>
                                </div>
                            </span>
                        </li>
                    </g:if>
                    <g:else>
                        <li class="">
                            <a href="${createLink([action: 'faq', controller: 'home'])}">
                                <i class="fa fa-chevron-left"></i>
                                <g:message code="navbar.menu.back.label"/>
                            </a>
                        </li>
                    </g:else>
                </ul>
            </div>
        </div>
    </div>
</div>



<!------------------------- Help Center NavBar ---------------------------------------->


<div id="helpcenter-nav" class="navbar navbar-default">
    <div class="">
        <div class="container">
            <a class="header__logo" href="${createLink([action: 'content', controller: 'home'])}">
                <img alt="Supporthero.io"
                     src="${assetPath(src: 'helpCenter/logo.png')}"/>
            </a>

            <div class="header__nav">
                <ul class="nav navbar-nav">
                    <li>
                        <g:link action="" controller="helpCenter">
                            <g:message code="help.navbar.help.title"/>
                        </g:link>
                        <div class="popover submenu ng-scope bottom am-flip-x"
                             style="top: 15px; left: -10px !important; display: block;">
                            <div class="arrow" style="left: 50%; margin-left: -11px; right: 0px;"></div>

                            <div class="submenu-content">
                                <g:link action="" controller="helpCenter">
                                    <g:message code="help.navbar.menu.tutorials"/>
                                </g:link>
                                <g:link action="article" controller="helpCenter">
                                    <g:message code="help.navbar.menu.articles"/>
                                </g:link>
                            </div>
                        </div>
                    </li>
                    <li>
                        <g:link action="statistic" controller="home">
                            <g:message code="help.navbar.contact.title"/>
                        </g:link>
                    </li>
                </ul>
            </div>

            <div class="pull-right header__nav">
                <ul class="nav pull-right navbar-nav">
                    <g:if test="${params.action != 'login'}">
                        <li>
                            <span class="lnk-drop" data-placement="bottom" data-animation="am-flip-x"
                                  data-template="signInPopover" data-bs-popover>
                                <g:message code="help.navbar.menu.signin"/>
                                <div class="signin-lnk__caret-wrapper">
                                    <span>
                                        <img alt="Supporthero.io"
                                             src="${assetPath(src: 'helpCenter/support.png')}"/>
                                    </span>
                                </div>
                            </span>
                            %{--login modal--}%
                            %{--<div class="popover ng-scope bottom am-flip-x"--}%
                            %{--style="top: 15px; left: -100px; display: block;">--}%
                            %{--<div class="arrow"></div>--}%

                            %{--<h3 class="popover-title"></h3>--}%

                            %{--<div class="popover-content">--}%
                            %{--<form autocomplete="on" class="form form-signin ng-pristine ng-valid"--}%
                            %{--name="loginForm" novalidate="">--}%

                            %{--<legend class="form__heading">Sign-In to Support Hero</legend>--}%
                            %{--<small class="detail">Please login to your account</small>--}%

                            %{--<div class="alert alert-error bg-danger ng-binding ng-hide"--}%
                            %{--data-ng-show="error.length > 0">--}%
                            %{--<i class="fa fa-exclamation-triangle"></i>--}%

                            %{--</div>--}%

                            %{--<div data-ng-class="{'has-error': loginForm.email.$invalid &amp;&amp; !loginForm.email.$pristine}">--}%
                            %{--<label class="control-label" for="email">--}%
                            %{--Email--}%
                            %{--</label>--}%

                            %{--<div>--}%
                            %{--<input class="form-control ng-pristine ng-valid ng-valid-email"--}%
                            %{--id="email" type="email" name="email" data-ng-model="email"--}%
                            %{--placeholder="Enter your email">--}%
                            %{--</div>--}%
                            %{--</div>--}%

                            %{--<div data-ng-class="{'has-error': loginForm.password.$invalid &amp;&amp; !loginForm.password.$pristine}">--}%
                            %{--<label for="password" class="control-label">--}%
                            %{--Password--}%
                            %{--</label>--}%

                            %{--<div>--}%
                            %{--<input class="form-control ng-pristine ng-valid" id="password"--}%
                            %{--type="password" name="password" data-ng-model="password"--}%
                            %{--placeholder="Enter your password">--}%
                            %{--</div>--}%
                            %{--</div>--}%

                            %{--<div class="controls clearfix">--}%
                            %{--<button class="btn btn-primary btn-helpcenter pull-left" data-ng-click="login()"--}%
                            %{--data-ng-disabled="submitting">--}%
                            %{--Save--}%
                            %{--</button>--}%
                            %{--<label class="checkbox pull-right">--}%
                            %{--<input class="form-control ng-pristine ng-valid" type="checkbox"--}%
                            %{--name="shAutoLogin"--}%
                            %{--data-ng-model="hasCookie"> Keep me signed in--}%
                            %{--</label>--}%
                            %{--</div>--}%

                            %{--<div class="popover-progress progress progress-striped active invisible"--}%
                            %{--data-ng-class="{invisible: !submitting}">--}%
                            %{--<div class="progress-bar" style="width: 100%;"></div>--}%
                            %{--</div>--}%
                            %{--</form>--}%

                            %{--<div class="popover-footer">--}%

                            %{--<a href="/login/forgotPassword"--}%
                            %{--data-ng-click="dismiss()">Forgot your Password?</a>--}%
                            %{--</div>--}%
                            %{--</div>--}%
                            %{--</div>--}%
                            %{--end login modal--}%
                        </li>
                    </g:if>
                    <g:else>
                        <li class="">
                            <a href="${createLink([action: 'faq', controller: 'home'])}">
                                <i class="fa fa-chevron-left"></i>
                                <g:message code="navbar.menu.back.label"/>
                            </a>
                        </li>
                    </g:else>
                </ul>
            </div>

        </div>
    </div>
</div>
</header>
