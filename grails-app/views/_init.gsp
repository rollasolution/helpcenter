<%@ page import="grails.util.Environment" %>
<script type="text/javascript">
    // <![CDATA[
    window.supporthero = {
        contextPath: '${request.contextPath}',
        <g:if test="${Environment.current != Environment.PRODUCTION}">debugEnabled: true,</g:if>
    };

    window.addEventListener('message', receiver, false);
    function receiver(e) {
        if (typeof e.data == 'string' && e.data.match('http')) {
            angular.element($('#supportHeroController')).scope().triggerParentUrlChanged(e.data);
        } else if (typeof e.data == 'string' && e.data.match('contact')) {
            angular.element($('#supportHeroController')).scope().triggerViewChanged(e.data);
        }
    }
    // ]]>
</script>