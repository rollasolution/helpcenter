%{--default text input to all views--}%

<div class="search-template ">
    <div class="input-group">
        <input type="text" class="form-control input-lg" placeholder="Search a question, a keyword, ...">
        <span class="input-group-btn btn-sample">
            <button class="btn btn-sample" type="button">Search</button>
        </span>
    </div>
</div>






