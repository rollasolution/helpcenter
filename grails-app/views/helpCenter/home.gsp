%{--Search header --}%
<div class="home-welcome col-lg-12 col-md-12 col-xs-12">
    <h1>How can we help?</h1>
    %{--default search bar--}%
    <div class="search-welcome center-block ">
        <g:render template="/searchHelpCenter"/>
    </div>
</div>
%{--Home Container--}%
<div class="container container-helpcenter col-lg-12 col-md-12 col-xs-12">
    <div class="col-lg-7 col-md-7 col-xs-7">
        %{--Popular Turorial--}%
        <div class="elements-container col-lg-10 col-md-10 col-xs-10">
            %{--Popular Turorial Header--}%
            <div class="default-title col-lg-10 col-md-10 col-xs-10">
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <h4>POPULAR TUTORIALS</h4>
                </div>

                <div class="col-lg-5 col-md-5 col-xs-5">
                    <a href="#" class="pull-right">BROWSE ALL<i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            <div class="video-conteiner col-lg-10 col-md-10 col-xs-10">
                <div ng-clickable="true" class="video-turorial col-lg-3 col-md-3 col-xs-10">
                    <a href="#">
                        <img src="${assetPath(src: 'helpCenter/video-tut.jpg')}"/>
                        <h6>TUTORIAL</h6>
                        <h5>How manage your page with a team</h5></a>
                </div>

                <div ng-clickable="true" class="video-turorial col-lg-3 col-md-3 col-xs-10">
                    <a href="#">
                        <img src="${assetPath(src: 'helpCenter/video-tut2.jpg')}"/>
                        <h6>TUTORIAL</h6>
                        <h5>Introduccion to the lastest twitter reports</h5></a>
                </div>

                <div ng-clickable="true" class="video-turorial col-lg-3 col-md-3 col-xs-10">
                    <a href="#">
                        <img src="${assetPath(src: 'helpCenter/video-tut.jpg')}"/>
                        <h6>TUTORIAL</h6>
                        <h5>Configure a Photo Contest with agorapulse</h5></a>
                </div>
            </div>
        </div>


        %{--POPULAR ARTICLES--}%
        <div class="elements-container col-lg-10 col-md-10 col-xs-10">
            %{--POPULAR ARTICLES Header--}%
            <div class="default-title col-lg-10 col-md-10 col-xs-10">
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <h4>POPULAR ARTICLES</h4>
                </div>

                <div class="col-lg-5 col-md-5 col-xs-5">
                    <a href="#" class="pull-right">BROWSE ALL<i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
            %{--POPULAR ARTICLES Items--}%
            <div class="default-items">
                <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
            </div>

            <div class="default-items">
                <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
            </div>

            <div class="default-items">
                <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
            </div>

            <div class="default-items">
                <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
            </div>
        </div>




        %{--GETTING STARTED --}%
        <div class="elements-container col-lg-10 col-md-10 col-xs-10">

            %{--GETTING STARTED Header--}%
            <div class="default-title col-lg-10 col-md-10 col-xs-10">
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <h4>GETTING STARTED</h4>
                </div>
            </div>

            %{--GETTING STARTED Elements--}%
            <div class="getting-title">
                <a>ACCOUNT</a> ( 5 )  <i class="fa fa-angle-right"></i>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>
            </div>

            <div class="getting-title">
                <a>FACEBOOK PAGE</a>  ( 8 )  <i class="fa fa-angle-right"></i>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>
            </div>


            <div class="getting-title">
                <a>TWITTER</a> ( 9 )  <i class="fa fa-angle-right"></i>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>
            </div>

            <div class="getting-title">
                <a>TIMELINE MANGEMENT</a> ( 14 )  <i class="fa fa-angle-right"></i>


                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>
            </div>

            <div class="getting-title">
                <a>PUBLISHING</a> ( 5 )  <i class="fa fa-angle-right"></i>


                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>
            </div>

            <div class="getting-title">
                <a>FAN MANGEMNET</a> ( 18 )  <i class="fa fa-angle-right"></i>


                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>What king of notifications will i receive
                </div>

                <div class="items">
                    <i class="fa fa-file-text-o default-icon"></i>How can i deactivate the notificatios of new post and comments
                </div>
            </div>
        </div>
    </div>

    <!-- Default sidebar help center-->
    <div class="main-sidebar col-lg-3 col-md-3 col-xs-3">
        <g:render template="/sidebarHelpCenter"/>
    </div>
</div>

