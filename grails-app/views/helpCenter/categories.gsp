<div class="container-helpcenter">

    <!-- default search bar-->
    <div class="def col-lg-12 col-md-12 col-xs-12">
        %{--default search bar--}%
        <div class="search-default pull-left col-lg-6 col-md-6 col-xs-6">
            <g:render template="/searchHelpCenter"/>
        </div>
    </div>

    %{--Categories Container--}%
    <div class="container col-lg-12 col-md-12 col-xs-12">

        <div class="categorie-content col-lg-7 col-md-7 col-xs-7">

            %{--Title Header--}%
            <div class="title-categorie">
                <h2 class="">Twitter</h2>
            </div>

            <div class="content-categorie">
                <h4>
                    <i class="fa fa-minus"></i> Twitter inbox
                </h4>
            </div>

            <div class="categorie-content-title">

                <div class="title">
                    <i class="fa fa-youtube-play cat-icon"></i>Twitter inbox
                </div>
            </div>


            <!-- Categories Items-->
            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>What's in my inbox
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i find all my dms, mentions o rts?
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>I'm trying to send a link throught dm and get an error message: why?
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i send a blank Twitter message?
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>how to reply to a direct message?
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i send a blank Twitter message?
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i reply to a mention
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i reply to a mention
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>

            <div class="categorie-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i see the whole conversation with multiple mentions and replies
            </div>
        </div>

        <!-- Default sidebar help center-->
        <div class="main-sidebar col-lg-3 col-md-3 col-xs-3">
            <g:render template="/sidebarHelpCenter"/>
        </div>
    </div>

</div>
