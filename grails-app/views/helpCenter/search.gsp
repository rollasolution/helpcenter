<div class="container container-helpcenter col-lg-12 col-md-12 col-xs-12">

    <div class="main-search col-lg-7 col-md-7 col-xs-7">
        %{--Search container--}%
        <div class="container-search">
            %{--default search bar--}%
            <div class="search-input">
                <g:render template="/searchHelpCenter"/>
            </div>
            %{--Search Title and View more results--}%
            <div class="search-title">
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <h3>67 Results</h3>
                </div>

                <div class="col-lg-5 col-md-5 col-xs-5">
                    <a href="#" class="pull-right">VIEW 5 MORE RESULTS<i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            %{--Search content--}%
            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>i don´t see any data in my <a>Twitter</a> report, why is that?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i add <a>Twitter</a> account?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>I want to send a DM to a user, but i can´t. Why?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How many <a>Twitter</a> accounts can i add?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>Can i export my <a>Twitter</a> report?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>is the teet limit computed per <a>Twitter</a> account?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>i don´t see any data in my <a>Twitter</a> report, why is that?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>What´s the user´s list on my <a>Twitter</a> account dashboard?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i> what´s the monitoring tool?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i add  <a>Twitter</a> account?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>I want to send a DM to a user, but i can´t. Why?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>i don´t see any data in my  <a>Twitter</a> report, why is that?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How many <a>Twitter</a> accounts can i add?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>i don´t see any data in my <a>Twitter</a> report, why is that?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How do i add <a>Twitter</a> account?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>I want to send a DM to a user, but i can´t. Why?
            </div>

            <div class="search-items">
                <i class="fa fa-file-text-o default-icon"></i>How many <a>Twitter</a> accounts can i add?
            </div>

        </div>
        %{--View more results--}%
        <div class="result-bottom">
            <a href="#" class="pull-right">VIEW 5 MORE RESULTS<i class="fa fa-long-arrow-right"></i></a>
        </div>
    </div>

    <!-- Default sidebar help center-->
    <div class="main-sidebar col-lg-3 col-md-3 col-xs-3">
        <g:render template="/sidebarHelpCenter"/>
    </div>
</div>
