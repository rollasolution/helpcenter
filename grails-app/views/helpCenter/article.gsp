<div class="container-helpcenter">
    <!-- default search bar-->
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="search-default pull-left col-lg-6 col-md-6 col-xs-6">
            <g:render template="/searchHelpCenter"/>
        </div>
    </div>
    %{--Article--}%
    <div class="container col-lg-12 col-md-12 col-xs-12">

        <div class="main-article row col-lg-7 col-md-7 col-xs-7">
            %{--Title Article--}%
            <div class="title-article">
                <h2>Why do you charge me in Euro?</h2>

                <div class="breadcrumbs-article">
                    <a href="#">Account
                        <i class="fa fa-angle-right article-icon"></i></a>
                    <a href="#">Subscription
                        <i class="fa fa-angle-right article-icon"></i></a>
                </div>
            </div>
            %{-- Content Article--}%

            <div class="content-article">
                <p>We invoice our clients in US Dollars or Euro. If you're in Europe, the system will automatically charge you
                in Euro and not in Dollars. Like you, we are based in Europe and the Euro is our native currency.
                As we expanded beyond the European borders, we had to offer a pricing in US Dollars, and we decided
                to keep the same numbers (i.e. 99€ became 99$, and not 125$).</p>


                <p>We've created the US Dollar pricing based on the Euro pricing, not the other way around.
                We are a startup with a strong European client base, and reducing our pricing in Euro would have
                had a dramatic impact on our bottom line as we pay our expenses in <a href="#">Euro, not US Dollars.</a>
                </p>

                <p>For each of you, this is a small difference, for us, this would have made a huge difference.</p>

                <p>We understand that you would prefer to pay 30% less (don't we all want to pay less ;-),
                but at this point, reducing our pricing would have too much of an impact on our bottom line.
                And even in Euro, you'll find that our pricing is still among the lowest on the market.</p>

                <p>We hope you understand our choice and think that Agorapulse is worth the price we are asking for it, even in Euro.</p>

                <p>Thank you for your understanding.</p>

                <p>If there is anything I can do to help you, don't hesitate to send me an email via our support system, I'll make sure we get back to you.</p>

                %{--LAst Updated Article--}%
                <p class="article-updated">Last Update :  1st march 2014   5:24 pm</p>
            </div>
        </div>

        <!-- Default sidebar help center-->
        <div class="main-sidebar col-lg-3 col-md-3 col-xs-3">
            <g:render template="/sidebarHelpCenter"/>
        </div>
    </div>
</div>