package supporthero.io

class HelpCenterController extends BaseController {

    static defaultAction = 'content'

    def beforeInterceptor = {
        log.debug "Before...$params.action"
        initializeContext()
    }

    def afterInterceptor = { model ->
        log.debug "After..."
    }

    def article() {}

    def categories() {}

    def home() {}

    def search() {}
}
