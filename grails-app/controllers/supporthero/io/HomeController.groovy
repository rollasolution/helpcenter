package supporthero.io

class HomeController extends BaseController {

    static defaultAction = 'content'

    def beforeInterceptor = {
        log.debug "Before...$params.action"
        initializeContext()
    }

    def afterInterceptor = { model ->
        log.debug "After..."
    }

    def content() {}

    def statistic() {}
}
