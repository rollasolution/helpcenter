package supporthero.io

class SupportHeroTagLib {

    static namespace = "supporthero"

    def init = { attrs ->
        Map model = [:]
        out << render(template: '/init', model: model)
    }

}
