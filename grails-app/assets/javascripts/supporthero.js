//= require libs/jquery/jquery
//= require libs/underscore/underscore
//= require libs/jquery-autosize/jquery.autosize
//= require libs/jquery-ui/jquery-ui
//= require libs/bootstrap-file-input/bootstrap.file-input
//= require libs/nprogress/nprogress
//= require libs/smartresize/jquery.smartresize
//= require_self
//= require base
//= require_tree controllers
//= require_tree directives
//= require_tree services
'use strict';

$(function() {
    // Implement data-hide, a replacement for data-dismiss (which destroys HTML elements in the DOM)
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });
});
// Create our namespace if it doesn't exist
if (!window.supporthero) {
    window.supporthero = {};
}