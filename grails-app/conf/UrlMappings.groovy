class UrlMappings {

    static excludes = ["/favicon.ico"]

	static mappings = {
        // Home
        '/'(controller: 'home', action: 'content')

        "/monitor"(controller: "monitor")

        "/widget/$action?"(controller: 'widget')

        "/import/$action?"(controller: 'import')

        "/$action?"(controller: 'home')/* {
            constraints {
                action inList: ['contact', 'faq', 'tutorial']
            }
        }*/

        //"/validate/email/$token"(controller: 'login', action: 'validateEmail')

        // Article
        "/article/show/$id"(action: 'show', controller: 'article')

        // Legacy urls
        "/question/show/$id"(action: 'show', controller: 'article')
        "/tutorial/show/$id"(action: 'show', controller: 'article')

        // Design
        "/design/$action"(controller: 'design')

        // Default
        "/$controller/$action?/$id?"{
            constraints {
                id matches: /\d+/
            }
        }

        '400'(controller: 'error', action: 'badRequest')
        '401'(controller: 'error', action: 'unauthorized')
        '403'(controller: 'error', action: 'forbidden')
        '404'(controller: 'error', action: 'notFound')
        '405'(controller: 'error', action: 'notAllowed')
        '408'(controller: 'error', action: 'timeOut')
        '500'(controller: 'error', action: 'error')
	}
}
