hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.region.factory_class = 'net.sf.ehcache.hibernate.SingletonEhCacheRegionFactory' // Singleton required when using multiple datasources

}
// environment specific settings
environments {
    development {
        // Defined in file:${userHome}/.grails/supporthero-config.groovy
    }
    test {
        // Defined in /etc/grails/supporthero-config.groovy
    }
    production {
        // Defined in /etc/grails/supporthero-config.groovy
    }
}
