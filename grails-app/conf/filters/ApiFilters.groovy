package filters

class ApiFilters {

    def filters = {
        api(uri: '/api/**') {
            before = {
                if (request.JSON) {
                    params << request.JSON
                }
            }
        }
    }
}
