package filters

class NavigationFilters {

    def filters = {
        angular(controller: '^(category)$') {
            // This filter must be removed to switch the navigation to a plain HTTP nav (not-ajaxy)
            before = {
                // Match request which are page reloads of HTML5 pushState URLs: not partial, on controllers where the
                // navigation is handled through Angular routes
                if (!params.containsKey('layout') && params.action != 'template') {
                    log.debug "Angular navigation redirect to home from (controller=${controllerName}, action=${actionName})"
                    redirect controller: 'home', action: 'faq',
                            params: [targetUri: request.forwardURI - request.contextPath]
                    return false
                }
            }
        }
    }
}
