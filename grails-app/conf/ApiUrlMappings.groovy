class ApiUrlMappings {

    static mappings = {

        "/api/account"(controller: 'accountApi') {
            action = [GET: 'show', PUT: 'update']
        }

        "/api/account/upload"(controller: 'accountApi')  {
            action = [POST: 'upload']
        }


        // Category API
        "/api/category/$id"(controller: 'categoryApi')  {
            action = [GET: 'show', PUT: 'update', DELETE: 'delete']
            constraints {
                id matches: /\d+/
            }
        }

        // Container API
        "/api/container"(controller: 'containerApi') {
            action = [GET: 'list', POST: 'create']
        }

        "/api/container/$id"(controller: 'containerApi')  {
            action = [GET: 'show', PUT: 'update', DELETE: 'delete']
            constraints {
                id matches: /\d+/
            }
        }
        "/api/container/content/$id"(controller: 'containerApi')  {
            action = [GET: 'fetch']
            constraints {
                id matches: /\d+/
            }
        }

        // Content API
        "/api/content/$action"(controller: 'ContentApi')

        // Home API
        "/api/home/$action"(controller: 'homeApi')

        // Import API
        "/api/import"(controller: 'importApi') {
            action = [POST: 'importFaq']
        }

        // Statistic API
        "/api/statistic/$action"(controller: 'statisticApi')

        // Url APIs
        "/api/url"(controller: 'urlApi') {
            action = [GET: 'list', POST: 'create']
        }

        "/api/url/$id"(controller: 'urlApi')  {
            action = [GET: 'show', PUT: 'update', DELETE: 'delete']
            constraints {
                id matches: /\d+/
            }
        }

        "/api/url/content/$id"(controller: 'urlApi')  {
            action = [GET: 'fetch']
            constraints {
                id matches: /\d+/
            }
        }

        "/api/url/$id/article/add/$articleId"(controller: 'urlApi')  {
            action = [PUT: 'addArticle', DELETE: 'removeArticle']
            constraints {
                id matches: /\d+/
                articleId matches: /\d+/
            }
        }

        "/api/topic/$action?"(controller: 'topicApi')

        "/api/formItem/$action/$parentId?"(controller: 'formItemApi')

        // Smart Contact Form APIs
        "/api/smartForm"(controller: 'smartFormApi') {
            action = [GET: 'show', POST: 'update']
        }

        "/api/contact"(controller: 'contactApi') {
            action = [POST: 'send']
        }

        "/api/contact/simple"(controller: 'contactApi') {
            action = [POST: 'simpleSend']
        }

        // Context API
        '/api/context'(controller: 'contextApi') {
            action = [GET: 'show']
        }

        // Tracking API
        '/api/track'(controller: 'analyticsApi') {
            action = [GET: 'trackWidgetOpened']
        }

        // Widget API
        "/api/widget/$action"(controller: 'widgetApi')

        // Search API
        "/api/search"(controller: 'searchApi') {
            action = [GET: 'search']
        }

        "/api/session/$action"(controller: 'authApi') {
            constraints {
                action inList: ['failure', 'loginInfo', 'logout', 'preLogin']
            }
        }

        // Article APIs
        "/api/article"(controller: 'articleApi') {
            action = [GET: 'list', POST: 'create']
        }

        "/api/article/upload"(controller: 'articleApi')  {
            action = [POST: 'upload']
        }

        "/api/article/$id"(controller: 'articleApi')  {
            action = [GET: 'show', PUT: 'update', DELETE: 'delete']
            constraints {
                id matches: /\d+/
            }
        }

        "/api/user"(controller: 'userApi') {
            action = [POST: 'create']
        }

        "/api/user/$id"(controller: 'userApi') {
            action = [GET: 'show', PUT: 'update']
            constraints {
                id matches: /\d+/
            }
        }

        // Ticket API
        "/api/ticket/$id"(controller: 'ticketApi')  {
            action = [GET: 'show', PUT: 'update', DELETE: 'delete']
            constraints {
                id matches: /\d+/
            }
        }

        // Analytics APIs
        "/api/analytics" (controller: 'analyticsApi') {
            action = [POST: 'save']
        }

    }

}